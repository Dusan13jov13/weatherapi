/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Location;
import entity.Weather;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jd140542d
 */
public class WeatherService {
    public static List<Weather> getAllWeathers() {
        List<Weather> list = new ArrayList<>();
        try {
            
            Connection c = db.DB.getInstance().getConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("SELECT w.Id,w.Date,l.id,l.lat,l.lon,l.city,l.state FROM Weather w, Location l where l.id = w.idloc");
            while (rs.next()) {
                Weather w = new Weather();
                Location l = new Location();
                w.setId(rs.getLong(1));
                w.setDate(rs.getDate(2).toLocalDate());
                l.setId(rs.getLong(3));
                l.setLat(rs.getFloat(4));
                l.setLon(rs.getFloat(5));
                l.setCity(rs.getString(6));
                l.setState(rs.getString(7));
                w.setLocation(l);
                Statement s2 = c.createStatement();
                ResultSet rs2 = s2.executeQuery("SELECT hour, temperature FROM temperature where idwea = " + w.getId());
                while (rs2.next()) {
                    int hour = rs2.getInt(1);
                    float temp = rs2.getFloat(2);
                    w.getTemperature()[hour] = temp;
                }
                s2.close();
                list.add(w);
            }
            s.close();
            db.DB.getInstance().putConnection(c);
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    } 
}
